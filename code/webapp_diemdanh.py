from flask import Flask,render_template
app = Flask(__name__)

@app.route('/mh')
def mh():
    return render_template('mh.html')

@app.route('/mh/login')
def login():
    return render_template('mh_login.html')

@app.route('/mh/login/verify')
def verify():
    return render_template('mh_verify.html')

@app.route('/mh/signup')
def signup():
    return render_template('mh_signup.html')

@app.route('/mh/main')
def main():
    return render_template('mh_main.html')


if __name__ == '__main__':
    app.run(debug=True)